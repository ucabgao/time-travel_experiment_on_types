### What is this repository for? ###

This repository records the complete history of an experiment on two static type systems for JavaScript, Flow and TypeScript. 

### How do I get set up? ###

There are 10 submodules in this repository, each of which has 40 bugs. Submodules IRA1 and IRA2 contain 80 bugs that are assessed by all three authors and used to calculate the inter-rater agreement; submodules from Experiment1 to Experiment8 contain 320 bugs that are assessed by one author. In each submodule, a branch is an individual effort that aims to prevent a public bug by adding type annotations under Flow or TypeScript. 